//     PC-GPIO
//     Copyright (C) 2018 ryukoposting
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contact:
//         email: epg@tfwno.gf

#ifndef PCGPIO_GLOBAL_H_
#define PCGPIO_GLOBAL_H_

/*
 * MICROCONTROLLER PREPROCESSOR STUFF
 */
//************************ PIC24FJ__GA002 ************************//
#if defined(__PIC24FJ16GA002__) || defined(__PIC24FJ32GA002__) || defined(__PIC24FJ48GA002__) || defined(__PIC24FJ64GA002__)
#define PCGPIO_TARGET_ARCHITECTURE_PIC24
#define PCGPIO_GLOBAL_MCU_ID 0x0100
#define PCGPIO_PIN_COUNT 17
#include "mcu/pcgpio_mcu.h"

#ifdef PCGPIO_SHOW_COMPILER_MESSAGES
#warning PCGPIO compiling for PIC24FJxxGA002.
#endif

//************************ PIC24FJ__GA004 ************************//
#elif defined(__PIC24FJ16GA004__) || defined(__PIC24FJ32GA004__) || defined(__PIC24FJ48GA004__) || defined(__PIC24FJ64GA004__)
#define PCGPIO_TARGET_ARCHITECTURE_PIC24
#define PCGPIO_GLOBAL_MCU_ID 0x0101
#define PCGPIO_PIN_COUNT 17
#include "mcu/pcgpio_mcu.h"

#ifdef PCGPIO_SHOW_COMPILER_MESSAGES
#warning PCGPIO compiling for PIC24FJxxGA004.
#endif

//******************* ARDUINO UNO/DUEMILANOVE ********************//
#elif defined(ARDUINO_AVR_UNO) || defined(ARDUINO_AVR_DUEMILANOVE)
#define PCGPIO_TARGET_ARCHITECTURE_ARDUINO_UNO
#define PCGPIO_GLOBAL_MCU_ID 0x0300
#define PCGPIO_PIN_COUNT 20
#include "mcu/pcgpio_mcu.h"

#ifdef PCGPIO_SHOW_COMPILER_MESSAGES
#pragma message("PCGPIO compiling for Arduino UNO/Duemilanove")
#endif

//************************** ARDUINO DUE *************************//
#elif defined(ARDUINO_SAM_DUE)
#define PCGPIO_TARGET_ARCHITECTURE_ARDUINO_DUE
#define PCGPIO_GLOBAL_MCU_ID 0x0301
#define PCGPIO_PIN_COUNT 72
#include "mcu/pcgpio_mcu.h"

#ifdef PCGPIO_SHOW_COMPILER_MESSAGES
#pragma message("PCGPIO compiling for Arduino DUE")
#endif

//************************ ARDUINO MEGA2560 **********************//
#elif defined(ARDUINO_AVR_MEGA2560)
#define PCGPIO_TARGET_ARCHITECTURE_ARDUINO_MEGA
#define PCGPIO_GLOBAL_MCU_ID 0x0302
#define PCGPIO_PIN_COUNT 70
#include "mcu/pcgpio_mcu.h"

#ifdef PCGPIO_SHOW_COMPILER_MESSAGES
#pragma message("PCGPIO compiling for Arduino Mega 2560")
#endif

//************************ ARDUINO MEGA ADK **********************//
#elif defined(ARDUINO_AVR_ADK)
#define PCGPIO_TARGET_ARCHITECTURE_ARDUINO_MEGA
#define PCGPIO_TARGET_ARCHITECTURE_ARGUINO_ADK
#define PCGPIO_GLOBAL_MCU_ID 0x0303
#define PCGPIO_PIN_COUNT 70
#include "mcu/pcgpio_mcu.h"

#ifdef PCGPIO_SHOW_COMPILER_MESSAGES
#pragma message("PCGPIO compiling for Arduino Mega ADK")
#endif

// NOTE: SAM3X must come after Arduino DUE, otherwise the Arduino IDE
// will pick this set of includes instead of the one specific to the DUE
//*************************** SAM3X_E ****************************//
#elif defined(__SAM3X8E__) || defined(__SAM3X4E__)
#define PCGPIO_TARGET_ARCHITECTURE_SAM3X
#define PCGPIO_GLOBAL_MCU_ID 0x0201
#include "mcu/pcgpio_mcu.h"

#ifdef PCGPIO_SHOW_COMPILER_MESSAGES
#pragma message("PCGPIO compiling for Atmel SAM3X4E/SAM3X8E")
#endif

/*
 * PC PREPROCESSOR STUFF
 */
#elif defined(_WIN32)    // defined for both 32-bit and 64-bit windows
#include "pc/pcgpio_host.h"

#elif defined(__unix__)  // linux, macos, *BSD
#include "pc/pcgpio_host.h"

#else // only occurs if no valid target MCU or OS was detected
#error Platform could not be determined.
#endif

#endif // PCGPIO_GLOBAL_H_
