//     PC-GPIO
//     Copyright (C) 2018 ryukoposting
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contact:
//         email: epg@tfwno.gf

#ifndef PCGPIO_HOST_H_
#define PCGPIO_HOST_H_

#include "../pcgpio_packet.h"

#if defined(__unix__)
#include <pthread.h>
#elif defined(_WIN32)
// haven't done the windows version yet...
#endif

#ifdef __cplusplus
extern "C" {
#endif


struct PCGPIO_Properties {
#if defined(__unix__)
    int fd;
    unsigned int state;
    struct PCGPIO_Callback receiver_callback;
    unsigned char checksum;
    int running;
    unsigned char rxbuffer[1024];
    int start, end;
    pthread_t rx_thread;
    void*(*pthread_function)(void*);
#endif
    void(*callback_handler)(struct PCGPIO_Properties *, struct PCGPIO_Callback);
    void(*error_handler)(struct PCGPIO_Properties *, enum PCGPIO_Status);
    unsigned int mcu_identity;
    char port_name[32];
};

/**
 * @brief Initialize a PCGPIO_Properties structure, and bind it to its corresponding port and interrupts.
 * 
 * Interrupt functions may NOT be shared between two PCGPIO_Properties structures, and a PCGPIO_Properties may
 * not be initialized twice.
 */
enum PCGPIO_Status PCGPIO_Init(struct PCGPIO_Properties *const properties, char *port_name, 
                               void(*callback_handler)(struct PCGPIO_Properties *properties, struct PCGPIO_Callback callback),
                               void(*error_handler)(struct PCGPIO_Properties *properties, enum PCGPIO_Status status));

/**
 * @brief Transmit a message on the serial bus with the properties specified by <i>method.</i>
 * 
 * This function is for advanced users. Normally, it is best to use method-specific functions:
 *   - @ref PCGPIO_RequestMCUID
 *   - @ref PCGPIO_IOConfig
 *   - @ref PCGPIO_WriteDigital
 *   - @ref PCGPIO_WriteAnalog
 *   - @ref PCGPIO_ReadDigital
 *   - @ref PCGPIO_ReadAnalog
 *   - @ref PCPGIO_GetPinConfig
 */
enum PCGPIO_Status PCGPIO_TransmitMethod(struct PCGPIO_Properties *const properties, struct PCGPIO_Method *const method);

enum PCGPIO_Status PCGPIO_RequestMCUID(struct PCGPIO_Properties *const properties);

enum PCGPIO_Status PCGPIO_IOConfig(struct PCGPIO_Properties *const properties, uint8_t pin, enum PCGPIO_PinConfig config);

enum PCGPIO_Status PCGPIO_WriteDigital(struct PCGPIO_Properties *const properties, uint8_t pin, uint16_t value);

enum PCGPIO_Status PCGPIO_WriteAnalog(struct PCGPIO_Properties *const properties, uint8_t pin, uint32_t value);

enum PCGPIO_Status PCGPIO_ReadDigital(struct PCGPIO_Properties *const properties, uint8_t pin);

enum PCGPIO_Status PCGPIO_ReadAnalog(struct PCGPIO_Properties *const properties, uint8_t pin);

enum PCGPIO_Status PCGPIO_GetPinConfig(struct PCGPIO_Properties *const properties, uint8_t pin);

char const *const PCGPIO_StatusAsString(enum PCGPIO_Status status);

#ifdef __cplusplus
}
#endif


#endif //PCGPIO_HOST_H_
