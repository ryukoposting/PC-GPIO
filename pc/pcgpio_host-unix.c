//     PC-GPIO
//     Copyright (C) 2018 ryukoposting
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contact:
//         email: epg@tfwno.gf

#include "pcgpio_host.h"
#include <termios.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <poll.h>
#include <errno.h>
#include <string.h>

char status_string[48];

static int PCGPIO_BufferPut___(struct PCGPIO_Properties *s, char c) {
    //if there is space in the buffer
    if ( s->end != ((s->start + 1023) % 1024)) {
        s->rxbuffer[s->end] = c;
        (s->end)++;
        s->end = s->end % 1024;
        return 0;
    }
    return -1;
}

static void PCGPIO_BufferClear___(struct PCGPIO_Properties *s) {
    s->start = 0;
    s->end = 0;
}

static unsigned char PCGPIO_GetNextChar___(struct PCGPIO_Properties *s) {
    unsigned char out = s->rxbuffer[s->start];
    if(++(s->start) >= 1024) s->start = 0;
    return out;
}

void *PCGPIO_ThreadHandler___(void* properties) {
    struct pollfd ufds;
    unsigned char buff[1024];
    
    struct PCGPIO_Properties *p = (struct PCGPIO_Properties*) properties;
    
    ufds.fd = p->fd;
    ufds.events = POLLIN;
    
    int poll_result;
    enum PCGPIO_Status error_found = PCGPIO_STATUS_OK;
    
    while(p->running != 0) {
        poll_result = poll(&ufds, 1, 2000);
        
        //
        if(poll_result > 0) {
            int cnt = read(p->fd, buff, 1023);
            
            if(cnt > 0) {
                buff[cnt] = '\0';
                
                for(int i = 0; i < cnt; i++) {
                    PCGPIO_BufferPut___(p, buff[i]);
                }
                
                
            } else if(cnt < 0) {
                // printf("serial port disconnected!\n");
                error_found = PCGPIO_STATUS_PORT_ERROR;
                p->error_handler(p, PCGPIO_STATUS_PORT_ERROR);
            } 
            
            while((p->end) != (p->start)) {
                // no new serial data on this cycle, work on whatever
                // is available in rxbuffer
                unsigned char in = PCGPIO_GetNextChar___(p);
                unsigned int next_state = 0x7FFF;  // 0x7FFF = error recovery
                
                switch(p->state) {
                    case 0:     // no message currently in transmission
                        if(in == 0xA5) {
                            p->checksum = 0;
                            next_state = 1;
                        } else {
                            next_state = 0;
                        }
                        break;
                    
                    case 1:     // got a start byte
                        p->receiver_callback.callback_type = (enum PCGPIO_CallbackType)in;
                        next_state = 0x100 + in;
                        p->checksum += in;
                        break;
                    
                    case 0x100: // first data byte of device identifier
                        p->receiver_callback.callback.mcu_identity.mcu_identifier = (in << 8);
                        next_state = 0x200;
                        p->checksum += in;
                        break;
                    case 0x200: // second data byte of device identifier
                        p->receiver_callback.callback.mcu_identity.mcu_identifier |= in;
                        next_state = 0xAAA;
                        p->checksum += in;
                        break;
                    
                    case 0x103: // first data byte of digital read
                        p->receiver_callback.callback.read_digital.pin = in;
                        next_state = 0x203;
                        p->checksum += in;
                        break;
                    case 0x203: // second data byte of digital read
                        p->receiver_callback.callback.read_digital.value = in;
                        next_state = 0xAAA;
                        p->checksum += in;
                        break;
                    
                    case 0x104: // first data byte of analog read
                        p->receiver_callback.callback.read_analog.pin = in;
                        next_state = 0x204;
                        p->checksum += in;
                        break;
                    case 0x204: // second data byte of analog read
                        p->receiver_callback.callback.read_analog.value = ((in & 0xFF) << 24);
                        next_state = 0x304;
                        p->checksum += in;
                        break;
                    case 0x304: // third data byte of analog read
                        p->receiver_callback.callback.read_analog.value |= ((in & 0xFF) << 16);
                        next_state = 0x404;
                        p->checksum += in;
                        break;
                    case 0x404: // fourth data byte of analog read
                        p->receiver_callback.callback.read_analog.value |= ((in & 0xFF) << 8);
                        next_state = 0x504;
                        p->checksum += in;
                        break;
                    case 0x504: // fifth data byte of analog read
                        p->receiver_callback.callback.read_analog.value |= (in & 0xFF);
                        next_state = 0xAAA;
                        p->checksum += in;
                        break;
                    
                    case 0x111: // first data byte of pinconf
                        p->receiver_callback.callback.get_pin_config.pin = in;
                        next_state = 0x211;
                        p->checksum += in;
                        break;
                    case 0x211: // first data byte of pinconf
                        p->receiver_callback.callback.get_pin_config.conf = in;
                        next_state = 0xAAA;
                        p->checksum += in;
                        break;
                    
                    case 0xAAA: // checksum byte
                        if((p->checksum) == (in & 0xFF)) {
                            next_state = 0xFFFF;
                        } else {
                            next_state = 0x7FFF;
                            p->error_handler(p, PCGPIO_STATUS_INCORRECT_CHECKSUM);
                        }
                        break;
                    
                    case 0x7FFF: // error recovery state
                        if(in == 0x0A) {
                            next_state = 0;
                        } else if(in == 0xA5) {
                            next_state = 1;
                        }
                        break;
                    
                    case 0xFFFF: // stop byte state
                        if(in == 0x0A) {
                            next_state = 0;
                            p->callback_handler(p, p->receiver_callback);
                        } else {
                            next_state = 0x7FFF;
                            p->error_handler(p, PCGPIO_STATUS_INCORRECT_STOPBYTE);
                        }
                        break;
                    
                    default:
                        next_state = 0x7FFF;
                        p->error_handler(p, PCGPIO_STATUS_INVALID_CALLBACK);
                }
                p->state = next_state;
            }
        } else if(poll_result < 0) {
            printf("polling error!\n");
            error_found = PCGPIO_STATUS_POLLING_ERROR;
            p->error_handler(p, PCGPIO_STATUS_POLLING_ERROR);
            break; // this kills the thread?
        }
        
        if(error_found) {
            p->running = 0;
        }
    }
    
    poll_result = close(p->fd);
    printf("serial port closing\n");
    
    return NULL;
}

unsigned char PCGPIO_MakeMethodChecksum(unsigned char* bytes, int length) {
    unsigned char out = 0;
    for(int i = 1; i < length - 2; i++) { //starts at 1 because ignoring start byte
        out += bytes[i];
    }
    return out;
}

enum PCGPIO_Status PCGPIO_Init(struct PCGPIO_Properties *const properties, char *port_name, 
                               void(*callback_handler)(struct PCGPIO_Properties *properties, struct PCGPIO_Callback callback),
                               void(*error_handler)(struct PCGPIO_Properties *properties, enum PCGPIO_Status status)) {
    if(properties == NULL) return PCGPIO_STATUS_NULL_PTR;
    if(strlen(port_name) > 32) return PCGPIO_STATUS_INVALID_PARAMETER;

    properties->pthread_function = &PCGPIO_ThreadHandler___;
    properties->callback_handler = callback_handler;
    properties->error_handler = error_handler;
    properties->state = 0;
    strncpy(properties->port_name, port_name, 32);
    
    // open port
    properties->fd = open(properties->port_name, O_RDWR | O_NOCTTY | O_NDELAY);
    if(properties->fd == -1) return PCGPIO_STATUS_PORT_ERROR;
    
    struct termios port_settings;
    
    tcgetattr(properties->fd, &port_settings);
    
    cfsetispeed(&port_settings, B115200);
    cfsetospeed(&port_settings, B115200);
    
    port_settings.c_cflag &= ~PARENB;
    port_settings.c_cflag &= ~CSTOPB;
    port_settings.c_cflag &= ~CSIZE;
    port_settings.c_cflag |= CS8;
    
    port_settings.c_cflag &= ~CRTSCTS;
    port_settings.c_cflag |= (CREAD | CLOCAL);
    port_settings.c_oflag &= ~OPOST;
    
    port_settings.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    
    if(tcsetattr(properties->fd, TCSANOW, &port_settings)) return PCGPIO_STATUS_PORT_ERROR;
    
    if(properties->running != 1) {
        properties->running = 1;
        int res = pthread_create(&properties->rx_thread, NULL, properties->pthread_function, (void*)properties);
    
        if(res) return PCGPIO_STATUS_THREAD_ERROR;
    }
    
    PCGPIO_RequestMCUID(properties);
    
    
    return PCGPIO_STATUS_OK;
}

enum PCGPIO_Status PCGPIO_TransmitMethod(struct PCGPIO_Properties *const properties, struct PCGPIO_Method *const method) {
    if(properties == NULL) return PCGPIO_STATUS_NULL_PTR;
    if(method == NULL) return PCGPIO_STATUS_NULL_PTR;
    if(properties->running != 1) return PCGPIO_STATUS_INVALID_PROPERTY;
    
    unsigned char packet_bytes[12] = {0xA5, };
    
    // the total length of the packet, including start, checksum and terminator
    unsigned int packet_length;
    
    switch(method->method_type) {
        case PCGPIO_METHODTYPE_MCUIDENTITY:
            packet_length = 4;
            packet_bytes[1] = (unsigned char)(method->method_type);
            packet_bytes[2] = PCGPIO_MakeMethodChecksum(packet_bytes, packet_length);
            packet_bytes[3] = 0x0A;
            break;
        case PCGPIO_METHODTYPE_IOCONFIG:
            packet_length = 6;
            packet_bytes[1] = (unsigned char)(method->method_type);
            packet_bytes[2] = method->method.io_pin_config.pin;
            packet_bytes[3] = method->method.io_pin_config.conf;
            packet_bytes[4] = PCGPIO_MakeMethodChecksum(packet_bytes, packet_length);
            packet_bytes[5] = 0x0A;
            break;
        case PCGPIO_METHODTYPE_WRITEDIGITAL:
            packet_length = 7;
            packet_bytes[1] = (unsigned char)(method->method_type);
            packet_bytes[2] = method->method.write_digital.pin;
            packet_bytes[3] = (uint8_t)(((method->method.write_digital.value) & 0xFF00) >> 8);
            packet_bytes[4] = (uint8_t)((method->method.write_digital.value) & 0x00FF);
            packet_bytes[5] = PCGPIO_MakeMethodChecksum(packet_bytes, packet_length);
            packet_bytes[6] = 0x0A;
            break;
        case PCGPIO_METHODTYPE_WRITEANALOG:
            packet_length = 9;
            packet_bytes[1] = (unsigned char)(method->method_type);
            packet_bytes[2] = method->method.write_analog.pin;
            packet_bytes[3] = (uint8_t)(((method->method.write_analog.value) & 0xFF000000) >> 24);
            packet_bytes[4] = (uint8_t)(((method->method.write_analog.value) & 0x00FF0000) >> 16);
            packet_bytes[5] = (uint8_t)(((method->method.write_analog.value) & 0x0000FF00) >> 8);
            packet_bytes[6] = (uint8_t)( (method->method.write_analog.value) & 0x000000FF);
            packet_bytes[7] = PCGPIO_MakeMethodChecksum(packet_bytes, packet_length);
            packet_bytes[8] = 0x0A;
            break;
        case PCGPIO_METHODTYPE_READDIGITAL:
            packet_length = 5;
            packet_bytes[1] = (unsigned char)(method->method_type);
            packet_bytes[2] = method->method.read_digital.pin;
            packet_bytes[3] = PCGPIO_MakeMethodChecksum(packet_bytes, packet_length);
            packet_bytes[4] = 0x0A;
            break;
        case PCGPIO_METHODTYPE_READANALOG:
            packet_length = 5;
            packet_bytes[1] = (unsigned char)(method->method_type);
            packet_bytes[2] = method->method.read_analog.pin;
            packet_bytes[3] = PCGPIO_MakeMethodChecksum(packet_bytes, packet_length);
            packet_bytes[4] = 0x0A;
            break;
        case PCGPIO_METHODTYPE_GETPINCONFIG:
            packet_length = 5;
            packet_bytes[1] = (unsigned char)(method->method_type);
            packet_bytes[2] = method->method.get_pin_config.pin;
            packet_bytes[3] = PCGPIO_MakeMethodChecksum(packet_bytes, packet_length);
            packet_bytes[4] = 0x0A;
            break;
        default:
            return PCGPIO_STATUS_INVALID_PROPERTY;
    }
    write(properties->fd, packet_bytes, packet_length);
    
    return PCGPIO_STATUS_OK;
}

char const *const PCGPIO_StatusAsString(enum PCGPIO_Status status) {
    switch(status) {
        case PCGPIO_STATUS_OK: 
            strcpy(status_string, "PCGPIO_STATUS_OK");
            break;
        case PCGPIO_STATUS_NULL_PTR: 
            strcpy(status_string, "PCGPIO_STATUS_NULL_PTR");
            break;
        case PCGPIO_STATUS_INVALID_PROPERTY: 
            strcpy(status_string, "PCGPIO_STATUS_INVALID_PROPERTY");
            break;
        case PCGPIO_STATUS_INVALID_CALLBACK: 
            strcpy(status_string, "PCGPIO_STATUS_INVALID_CALLBACK");
            break;
        case PCGPIO_STATUS_INVALID_PARAMETER: 
            strcpy(status_string, "PCGPIO_STATUS_INVALID_PARAMETER");
            break;
        case PCGPIO_STATUS_PORT_ERROR:
            strcpy(status_string, "PCGPIO_STATUS_PORT_ERROR");
            break;
        case PCGPIO_STATUS_THREAD_ERROR:
            strcpy(status_string, "PCGPIO_STATUS_THREAD_ERROR");
            break;
        case PCGPIO_STATUS_POLLING_ERROR:
            strcpy(status_string, "PCGPIO_STATUS_POLLING_ERROR");
            break;
        case PCGPIO_STATUS_INCORRECT_CHECKSUM:
            strcpy(status_string, "PCGPIO_STATUS_INCORRECT_CHECKSUM");
            break;
        case PCGPIO_STATUS_INCORRECT_STOPBYTE:
            strcpy(status_string, "PCGPIO_STATUS_INCORRECT_STOPBYTE");
            break;
        case PCGPIO_STATUS_IN_RECOVERY_STATE:
            strcpy(status_string, "PCGPIO_STATUS_IN_RECOVERY_STATE");
            break;
        case PCGPIO_STATUS_INTERNAL_FAILURE:
            strcpy(status_string, "PCGPIO_STATUS_INTERNAL_FAILURE");
            break;
        default:
            strcpy(status_string, "UNDEFINED");
    }
    return status_string;
}
