//     PC-GPIO
//     Copyright (C) 2018 ryukoposting
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contact:
//         email: epg@tfwno.gf

#include "pcgpio_host.h"

enum PCGPIO_Status PCGPIO_RequestMCUID(struct PCGPIO_Properties *const properties) {
    struct PCGPIO_METHOD_MCUIdentity mcu_identity = {
        
    };
    struct PCGPIO_Method method = {
        .method_type = PCGPIO_METHODTYPE_MCUIDENTITY
    };
    method.method.mcu_identity = mcu_identity;
    return PCGPIO_TransmitMethod(properties, &method);
}

enum PCGPIO_Status PCGPIO_IOConfig(struct PCGPIO_Properties *const properties, uint8_t pin, enum PCGPIO_PinConfig config) {
    struct PCGPIO_METHOD_IOPinConfig io_pin_config = {
        .pin = pin,
        .conf = (uint8_t)config
    };
    struct PCGPIO_Method method = {
        .method_type = PCGPIO_METHODTYPE_IOCONFIG
    };
    method.method.io_pin_config = io_pin_config;
    return PCGPIO_TransmitMethod(properties, &method);
}

enum PCGPIO_Status PCGPIO_WriteDigital(struct PCGPIO_Properties *const properties, uint8_t pin, uint16_t value) {
    struct PCGPIO_METHOD_WriteDigital write_digital = {
        .pin = pin,
        .value = value
    };
    struct PCGPIO_Method method = {
        .method_type = PCGPIO_METHODTYPE_WRITEDIGITAL
    };
    method.method.write_digital = write_digital;
    return PCGPIO_TransmitMethod(properties, &method);
}

enum PCGPIO_Status PCGPIO_WriteAnalog(struct PCGPIO_Properties *const properties, uint8_t pin, uint32_t value) {
    struct PCGPIO_METHOD_WriteAnalog write_analog = {
        .pin = pin,
        .value = value
    };
    struct PCGPIO_Method method = {
        .method_type = PCGPIO_METHODTYPE_WRITEANALOG
    };
    method.method.write_analog = write_analog;
    return PCGPIO_TransmitMethod(properties, &method);
}

enum PCGPIO_Status PCGPIO_ReadDigital(struct PCGPIO_Properties *const properties, uint8_t pin) {
    struct PCGPIO_METHOD_ReadDigital read_digital = {
        .pin = pin
    };
    struct PCGPIO_Method method = {
        .method_type = PCGPIO_METHODTYPE_READDIGITAL
    };
    method.method.read_digital = read_digital;
    return PCGPIO_TransmitMethod(properties, &method);
}

enum PCGPIO_Status PCGPIO_ReadAnalog(struct PCGPIO_Properties *const properties, uint8_t pin) {
    struct PCGPIO_METHOD_ReadAnalog read_analog = {
        .pin = pin
    };
    struct PCGPIO_Method method = {
        .method_type = PCGPIO_METHODTYPE_READANALOG
    };
    method.method.read_analog = read_analog;
    return PCGPIO_TransmitMethod(properties, &method);
}

enum PCGPIO_Status PCGPIO_GetPinConfig(struct PCGPIO_Properties *const properties, uint8_t pin) {
    struct PCGPIO_METHOD_GetPinConfig get_pin_config = {
        .pin = pin
    };
    struct PCGPIO_Method method = {
        .method_type = PCGPIO_METHODTYPE_GETPINCONFIG
    };
    method.method.get_pin_config = get_pin_config;
    return PCGPIO_TransmitMethod(properties, &method);
}
