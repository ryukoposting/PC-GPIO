//     PC-GPIO
//     Copyright (C) 2018 ryukoposting
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contact:
//         email: epg@tfwno.gf

#ifndef PCGPIO_CALLBACK_H_
#define PCGPIO_CALLBACK_H_

#include "pcgpio_global.h"
#include <stdint.h>

enum PCGPIO_CallbackType {
    PCGPIO_CALLBACKTYPE_MCUIDENTITY = 0x00U,
    PCGPIO_CALLBACKTYPE_READDIGITAL = 0x03U,
    PCGPIO_CALLBACKTYPE_READANALOG = 0x04U,
    PCGPIO_CALLBACKTYPE_GETPINCONFIG = 0x11U
};

struct PCGPIO_CALLBACK_MCUIdentity {
    uint16_t mcu_identifier;
};

struct PCGPIO_CALLBACK_ReadDigital {
    uint8_t pin;
    uint8_t value;
};

struct PCGPIO_CALLBACK_ReadAnalog {
    uint8_t pin;
    uint32_t value;
};

struct PCGPIO_CALLBACK_GetPinConfig {
    uint8_t pin;
    uint8_t conf;
};

struct PCGPIO_Callback {
    enum PCGPIO_CallbackType callback_type;
    union {
        struct PCGPIO_CALLBACK_MCUIdentity mcu_identity;
        struct PCGPIO_CALLBACK_ReadDigital read_digital;
        struct PCGPIO_CALLBACK_ReadAnalog read_analog;
        struct PCGPIO_CALLBACK_GetPinConfig get_pin_config;
    } callback;
};

#endif
