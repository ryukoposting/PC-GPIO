**[Get the daily build here!](http://dailyprog.org/~ryukoposting/projects/PC-GPIO/)**
### PC-GPIO: API for manipulating microcontroller IO from your computer

The PC-GPIO API is a multi-platform API for interfacing with microcontroller IO from a
host computer running Windows, OSX, Linux, or BSD. The host computer talks to the
microcontroller over TTL serial at 115.2kbaud.

Currently, the API allows the user to:
- Use any number of microcontrollers simultaneously
- Read and write to digital IO pins
- Read from analog input pins
- Write to PWM output pins
- Configure input capture with automatic callbacks to the host computer on capture

Planned features:
- Configure and interface with I2C, SPI, UART, and CAN ports
- Write to Analog outputs (for MCUs that have DACs built in)
- Special function set for MCU-specific features

PC-GPIO is...
- Lightweight: default PC-GPIO microcontroller firmware generally uses very little of the
    slave microcontroller's resources. Customized firmware is easy to make, and can utilize
    a sizable amount of the microcontroller's resources without interfering with PC-GPIO.
- Cross-compatible: PC-GPIO requires no heap-allocated memory, and communicates over
    standard TTL serial, meaning it is compatible with virtually any modern microcontroller.
    The host-side PC-GPIO API has absolutely no external dependencies except for the
    low-level libraries provided by the user's operating system.
- Industrial-Strength: error and safety checks abound in the PC-GPIO API, with virtually
    all functions returning some form of status message. Checksums on every message mean
    that errors are found before they can have unwanted effects on IO. Because of the lack
    of heap usage, risk of memory fragmentation is eliminated, which is helpful in
    environments where indefinite uptime is essential.
- Asynchronous: Send methods to the microcontroller, get callbacks from the microcontroller
    in a user-defined handler function.

### Supported Operating Systems
- Currently Supported:
    - The UNIXes, including Linux, OSX, and the BSD's
- Planned:
    - Windows (framework is already there, just need to write the platform-specific code)

### Natively Supported Microcontrollers
Literally any microcontroller with a UART peripheral that can run at 115200 baud can be
used with PC-GPIO with just a few dozen lines of code. However, implementations already
exist for the microcontrollers listed below:

- Arduino
    - DUE: fully supported. Supported natively using firmware that will be uploaded
      sometime in the coming weeks, and will be updated as additions to the API are made.

For the time being, support for PIC microcontrollers is on the back burner.
Microchip's hot garbage MPLAB X IDE's automatic makefile generation is insufficient for
PC-GPIO's multi-platform library architecture, and it does not allow the user to directly
manipulate the makefiles in the project without the changes being overwritten.

### Using the Library
Adding PC-GPIO to a CMake project:
1. Assuming the library is in a subdirectory of your project named 'PC-GPIO':

```cmake
cmake_minimum_required(VERSION 3.5)
project(your_project_name LANGUAGES C)
set(CMAKE_C_STANDARD 11)

add_subdirectory(PC-GPIO)

set(CMAKE_EXE_LINKER_FLAGS "-pthread")

add_executable(your_project_name your_project_main.c)

target_link_libraries(your_project_name LINK_PUBLIC pcgpio)

```

Adding PC-GPIO to an Arduino project:
1. Download the complete PC-GPIO source.
2. Open your Arduino project folder (the one that includes the .ino file).
3. In this directory, extract the PC-GPIO source into a folder called "PC-GPIO"
   So there will be 2 things in the project folder, the .ino file and a folder
   called PC-GPIO.
4. Import PC-GPIO into your arduino project by adding `#include "PC-GPIO/pcgpio_global.h"`
   To the top of your arduino code.

### License
Copyright (C) 2018 ryukoposting

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

Contact: [epg@tfwno.gf](mailto:epg@tfwno.gf)
