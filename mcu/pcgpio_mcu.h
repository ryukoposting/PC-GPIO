//     PC-GPIO
//     Copyright (C) 2018 ryukoposting
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
//     Contact:
//         email: epg@tfwno.gf

#ifndef PCGPIO_MCU_H_
#define PCGPIO_MCU_H_

#include "../pcgpio_packet.h"

#ifdef __cplusplus
extern "C" {
#endif

uint8_t pin_modes[PCGPIO_PIN_COUNT] = {};
uint8_t pin_last_scan[PCGPIO_PIN_COUNT] = {};

struct PCGPIO_Method receiver_method;

enum PCGPIO_Status PCGPIO_Start();

enum PCGPIO_Status PCGPIO_ProcessNextByte(uint8_t in);

enum PCGPIO_Status PCGPIO_TransmitCallback(struct PCGPIO_Callback *callback);

enum PCGPIO_Status PCGPIO_ProcessMethod(struct PCGPIO_Method *method);

enum PCGPIO_Status PCGPIO_SendMCUID();

enum PCGPIO_Status PCGPIO_SendDigitalInputRead(uint8_t pin, uint8_t value);

enum PCGPIO_Status PCGPIO_SendAnalogInputRead(uint8_t pin, uint32_t value);

enum PCGPIO_Status PCGPIO_SendPinConfig(uint8_t pin, uint8_t conf);


#ifdef __cplusplus
}
#endif

// HACK HACK HACK HACK HACK FOR THE LOVE OF GOD FIGURE OUT ANOTHER WAY
#if defined(PCGPIO_TARGET_ARCHITECTURE_ARDUINO_DUE)
#include "pcgpio_mcu-common.c"
#include "arduino/pcgpio_mcu-due.c"
#elif defined (PCGPIO_TARGET_ARCHITECTURE_ARDUINO_UNO)
#include "pcgpio_mcu.h-common.c"
#include "arduino/pcgpio_mcu-due.c"
#endif

#endif // PCGPIO_MCU_H_
