//     PC-GPIO
//     Copyright (C) 2018 ryukoposting
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contact:
//         email: epg@tfwno.gf

#if defined(PCGPIO_TARGET_ARCHITECTURE_ARDUINO_UNO)
#pragma message("found implementation for UNO")
#include "../pcgpio_mcu.h"

// due pin numbers: all digital pins same as what's printed on the board
// analog pins: 14 + n

SIGNAL (TIMER0_COMA_vect){
    // digital IO
    for(int i = 0; i < 20; i++) {
        uint8_t setting = pin_modes[i] & 0xF0;
        switch(setting) {
            case 0:
                if((pin_modes[i] & 0x0C) == 0x0C) { // both edge capture
                    uint8_t read = digitalRead(i);
                    if(read != pin_last_scan[i]) {
                        PCGPIO_SendDigitalInputRead(i, read);
                    }
                    pin_last_scan[i] = read;
                } else if(pin_modes[i] & 0x04) { // rising edge capture
                    uint8_t read = digitalRead(i);
                    if(read > pin_last_scan[i]) {
                        PCGPIO_SendDigitalInputRead(i, read);
                    }
                    pin_last_scan[i] = read;
                } else if(pin_modes[i] & 0x08) { // falling edge capture
                    uint8_t read = digitalRead(i);
                    if(read < pin_last_scan[i]) {
                        PCGPIO_SendDigitalInputRead(i, read);
                    }
                    pin_last_scan[i] = read;
                }
                break;
                // nothing else currently needed for this, 
                // more stuff may be here at some point in the future
        }
    }
    
}

enum PCGPIO_Status PCGPIO_Start() {
    analogWriteResolution(16);
    Serial.begin(115200);
    state = 0;
    receiver_method = {};
    
    OCR0A = 0xAF;
    TIMSK0 |= _BV(OCIE0A);
    
    for(int i = 0; i < 62; i++) {
        // special case state used for debugging erroneous captures
        pin_last_scan[i] = 0xF0;
    }
    
    return PCGPIO_STATUS_OK;
}

enum PCGPIO_Status PCGPIO_TransmitCallback(struct PCGPIO_Callback *callback) {
    uint8_t buffer[16] = {0xA5, callback->callback_type, };
    int length = 0;
    switch(callback->callback_type) {
        case PCGPIO_CALLBACKTYPE_MCUIDENTITY:
            length = 6;
            buffer[2] = (((callback->callback.mcu_identity.mcu_identifier) >> 8) & 0xFF);
            buffer[3] = ((callback->callback.mcu_identity.mcu_identifier) & 0xFF);
            buffer[4] = ((callback->callback_type + buffer[2] + buffer[3]) & 0xFF);
            buffer[5] = 0x0A;
            break;
        case PCGPIO_CALLBACKTYPE_READDIGITAL:
            length = 6;
            buffer[2] = callback->callback.read_digital.pin;
            buffer[3] = callback->callback.read_digital.value;
            buffer[4] = ((callback->callback_type + buffer[2] + buffer[3]) & 0xFF);
            buffer[5] = 0x0A;
            break;
        case PCGPIO_CALLBACKTYPE_READANALOG:
            length = 9;
            buffer[2] = callback->callback.read_analog.pin;
            buffer[3] = ((callback->callback.read_analog.value >> 24) & 0xFF);
            buffer[4] = ((callback->callback.read_analog.value >> 16) & 0xFF);
            buffer[5] = ((callback->callback.read_analog.value >> 8) & 0xFF);
            buffer[6] = callback->callback.read_analog.value & 0xFF;
            buffer[7] = ((buffer[1] + buffer[2] + buffer[3] + buffer[4] + buffer[5] + buffer[6]) & 0xFF);
            buffer[8] = 0x0A;
            break;
        case PCGPIO_CALLBACKTYPE_GETPINCONFIG:
            length = 6;
            buffer[2] = callback->callback.get_pin_config.pin;
            buffer[3] = callback->callback.get_pin_config.conf;
            buffer[4] = ((callback->callback_type + buffer[2] + buffer[3]) & 0xFF);
            buffer[5] = 0x0A;
            break;
        default:
            return PCGPIO_STATUS_INVALID_CALLBACK;
    }
    Serial.write(buffer, length);
    return PCGPIO_STATUS_OK;
}

enum PCGPIO_Status PCGPIO_ProcessMethod(struct PCGPIO_Method *method) {
    struct PCGPIO_Callback callback;

    switch(method->method_type) {
        case PCGPIO_METHODTYPE_MCUIDENTITY: {
                callback.callback_type = PCGPIO_CALLBACKTYPE_MCUIDENTITY;
                struct PCGPIO_CALLBACK_MCUIdentity mcu_identity = {
                    .mcu_identifier = PCGPIO_GLOBAL_MCU_ID
                };
                callback.callback.mcu_identity = mcu_identity;
                PCGPIO_TransmitCallback(&callback);
            }
            break;
        case PCGPIO_METHODTYPE_IOCONFIG:
            if((method->method.io_pin_config.pin) < 54) {
                if((method->method.io_pin_config.conf & 0xF0) == 0) { // is an input
                    // pullup/pulldown configuration
                    switch((method->method.io_pin_config.conf) & 0x03) {
                        case 0: // no pullups or pulldowns
                            pinMode(method->method.io_pin_config.pin, INPUT);
                            pin_modes[method->method.io_pin_config.pin] = method->method.io_pin_config.conf;
                            break;
                        case 1: // pulldown
                            /* not available on DUE */
                            break;
                        case 2: // pullup
                            pinMode(method->method.io_pin_config.pin, INPUT_PULLUP);
                            pin_modes[method->method.io_pin_config.pin] = method->method.io_pin_config.conf;
                            break;
                    }
                    // input capture on DUE is handled in interrupt
                    if((method->method.io_pin_config.conf) & 0x0C) {
                        pin_last_scan[method->method.io_pin_config.pin] = digitalRead(method->method.io_pin_config.pin);
                    }
                } else if((method->method.io_pin_config.conf & 0xF0) == 0x10) { // is an output
                    if((method->method.io_pin_config.conf == 0x17) || (method->method.io_pin_config.conf == 0x10)) {
                        // DUE doesn't implement open drain outputs, and PWM config is
                        // the same as normal outputs
                        pinMode(method->method.io_pin_config.pin, OUTPUT);
                        pin_modes[method->method.io_pin_config.pin] = method->method.io_pin_config.conf;
                    }
                }
            } else {
                
            }
            break;
        case PCGPIO_METHODTYPE_WRITEDIGITAL:
            if(method->method.write_digital.pin < 14) {
                if(pin_modes[(method->method.write_digital.pin)] == 0x17) {
                    // is a PWM pin
                    analogWrite(method->method.write_digital.pin, method->method.write_digital.value);
                } else {
                    digitalWrite(method->method.write_digital.pin, method->method.write_digital.value);
                }
            }
            break;
        case PCGPIO_METHODTYPE_WRITEANALOG:
            
            break;
        case PCGPIO_METHODTYPE_READDIGITAL: 
            if(method->method.read_digital.pin < 14) {
                callback.callback_type = PCGPIO_CALLBACKTYPE_READDIGITAL;
                struct PCGPIO_CALLBACK_ReadDigital read_digital = {
                    .pin = (method->method.read_digital.pin),
                    .value = digitalRead(method->method.read_digital.pin)
                };
                callback.callback.read_digital = read_digital;
                PCGPIO_TransmitCallback(&callback);
            }
            break;
        case PCGPIO_METHODTYPE_READANALOG:
            if((method->method.read_digital.pin >= 14) && (method->method.read_digital.pin < 20)) {
                callback.callback_type = PCGPIO_CALLBACKTYPE_READANALOG;
                struct PCGPIO_CALLBACK_ReadAnalog read_analog = {
                    .pin = (method->method.read_digital.pin),
                    .value = analogRead(method->method.read_digital.pin)
                };
                callback.callback.read_analog = read_analog;
                PCGPIO_TransmitCallback(&callback);
            }
            break;
        case PCGPIO_METHODTYPE_GETPINCONFIG:
            break;
        default:
            return PCGPIO_STATUS_INVALID_METHOD;
    }
    
    return PCGPIO_STATUS_OK;
}

#endif
