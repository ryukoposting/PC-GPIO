#include "pcgpio_mcu.h"

static uint16_t state;
static uint16_t next_state;
static uint8_t checksum;

enum PCGPIO_Status PCGPIO_SendMCUID() {
    struct PCGPIO_Callback callback = {
        .callback_type = PCGPIO_CALLBACKTYPE_MCUIDENTITY
    };
    struct PCGPIO_CALLBACK_MCUIdentity mcu_identity = {
        .mcu_identifier = PCGPIO_GLOBAL_MCU_ID
    };
    callback.callback.mcu_identity = mcu_identity;
    return PCGPIO_TransmitCallback(&callback);
}

enum PCGPIO_Status PCGPIO_ProcessNextByte(uint8_t in) {
    enum PCGPIO_Status retval = PCGPIO_STATUS_OK;
    switch(state) {
        case 0:     // no message currently in transmission
            if(in == 0xA5) {
                checksum = 0;
                next_state = 1;
            } else {
                next_state = 0;
            }
            break;

        case 1:     // got a start byte. figure out what method it is
            receiver_method.method_type = (enum PCGPIO_MethodType)in;
            next_state = 0x100 + in;
            checksum += in;
            break;

        case 0x100: // checksum byte
            if(checksum == (in & 0xFF)) {
                next_state = 0xFFFF;
            } else {
                next_state = 0x7FFF;
                retval = PCGPIO_STATUS_INCORRECT_CHECKSUM;
            }
            break;

        case 0x101:  // first data byte of IOPinConfig method
            receiver_method.method.io_pin_config.pin = in;
            checksum += in;
            next_state = 0x201;
            break;
        case 0x201:  // second data byte of IOPinConfig method
            receiver_method.method.io_pin_config.conf = in;
            checksum += in;
            next_state = 0x100;
            break;

        case 0x102:  // first data byte of WriteDigital method
            receiver_method.method.write_digital.pin = in;
            checksum += in;
            next_state = 0x202;
            break;
        case 0x202:  // second data byte of WriteDigital method
            receiver_method.method.write_digital.value = (in << 8);
            checksum += in;
            next_state = 0x302;
            break;
        case 0x302:  // third data byte of WriteDigital method
            receiver_method.method.write_digital.value |= (in & 0x00FF);
            checksum += in;
            next_state = 0x100;
            break;

        case 0x103:  // first data byte of ReadDigital method
            receiver_method.method.read_digital.pin = in;
            checksum += in;
            next_state = 0x100;
            break;

        case 0x104:  // first data byte of ReadAnalog method
            receiver_method.method.read_analog.pin = in;
            checksum += in;
            next_state = 0x100;
            break;
        
        case 0x105:  // first data byte of WriteAnalog method
            receiver_method.method.write_analog.pin = in;
            checksum += in;
            next_state = 0x205;
            break;
        case 0x205:
            receiver_method.method.write_analog.value = (in << 24);
            checksum += in;
            next_state = 0x305;
            break;
        case 0x305:
            receiver_method.method.write_analog.value |= (in << 16);
            checksum += in;
            next_state = 0x405;
            break;
        case 0x405:
            receiver_method.method.write_analog.value |= (in << 8);
            checksum += in;
            next_state = 0x505;
            break;
        case 0x505:
            receiver_method.method.write_analog.value |= in;
            checksum += in;
            next_state = 0x100;
            break;

        case 0x111:  // first data byte of GetPinConfig method
            receiver_method.method.read_analog.pin = in;
            checksum += in;
            next_state = 0x100;
            break;

        case 0x7FFF: // error recovery state
            if(in == 0x0A) {
                next_state = 0;
            } else if(in == 0xA5) {
                next_state = 1;
            } else {
                // state stays the same
                retval = PCGPIO_STATUS_IN_RECOVERY_STATE;
            }
            break;

        case 0xFFFF: // should get stop byte
            if(in == 0x0A) {
                // we have gotten a complete, properly formatted method!
                state = 0;
                return PCGPIO_ProcessMethod(&receiver_method);
            } else {
                next_state = 0x7FFF;
                retval = PCGPIO_STATUS_INCORRECT_STOPBYTE;
            }
            break;

        default:
            next_state = 0x7FFF;
            retval = PCGPIO_STATUS_INTERNAL_FAILURE;
    }

    state = next_state;
    return retval;
}

enum PCGPIO_Status PCGPIO_SendDigitalInputRead(uint8_t pin, uint8_t value) {
    struct PCGPIO_Callback callback = {
        .callback_type = PCGPIO_CALLBACKTYPE_READDIGITAL
    };
    struct PCGPIO_CALLBACK_ReadDigital read_digital = {
        .pin = pin,
        .value = value
    };
    callback.callback.read_digital = read_digital;
    return PCGPIO_TransmitCallback(&callback);
}

enum PCGPIO_Status PCGPIO_SendAnalogInputRead(uint8_t pin, uint32_t value) {
    struct PCGPIO_Callback callback = {
        .callback_type = PCGPIO_CALLBACKTYPE_READANALOG
    };
    struct PCGPIO_CALLBACK_ReadAnalog read_analog = {
        .pin = pin,
        .value = value
    };
    callback.callback.read_analog = read_analog;
    return PCGPIO_TransmitCallback(&callback);
}

enum PCGPIO_Status PCGPIO_SendPinConfig(uint8_t pin, uint8_t conf) {
    struct PCGPIO_Callback callback = {
        .callback_type = PCGPIO_CALLBACKTYPE_GETPINCONFIG
    };
    struct PCGPIO_CALLBACK_GetPinConfig get_pin_config = {
        .pin = pin,
        .conf = conf
    };
    callback.callback.get_pin_config = get_pin_config;
    return PCGPIO_TransmitCallback(&callback);
}
