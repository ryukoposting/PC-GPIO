//     PC-GPIO
//     Copyright (C) 2018 ryukoposting
// 
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
// 
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
// 
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//     
//     Contact:
//         email: epg@tfwno.gf

#ifndef PCGPIO_METHOD_H_
#define PCGPIO_METHOD_H_

#include "pcgpio_global.h"
#include <stdint.h>

enum PCGPIO_MethodType {
    PCGPIO_METHODTYPE_MCUIDENTITY  = 0x00U,
    PCGPIO_METHODTYPE_IOCONFIG     = 0x01U,
    PCGPIO_METHODTYPE_WRITEDIGITAL = 0x02U,
    PCGPIO_METHODTYPE_READDIGITAL  = 0x03U,
    PCGPIO_METHODTYPE_READANALOG   = 0x04U,
    PCGPIO_METHODTYPE_WRITEANALOG  = 0x05U,
    PCGPIO_METHODTYPE_GETPINCONFIG = 0x11U
};

struct PCGPIO_METHOD_MCUIdentity {
    //MCUIdentity method takes no data
    //but it's still a method that exists
};

struct PCGPIO_METHOD_IOPinConfig {
    uint8_t pin;
    uint8_t conf;
};

struct PCGPIO_METHOD_WriteDigital {
    uint8_t pin;
    uint16_t value;
};

struct PCGPIO_METHOD_WriteAnalog {
    uint8_t pin;
    uint32_t value;
};

struct PCGPIO_METHOD_ReadDigital {
    uint8_t pin;
};

struct PCGPIO_METHOD_ReadAnalog {
    uint8_t pin;
};

struct PCGPIO_METHOD_GetPinConfig {
    uint8_t pin;
};

struct PCGPIO_Method {
    enum PCGPIO_MethodType method_type;
    union {
        struct PCGPIO_METHOD_MCUIdentity mcu_identity;
        struct PCGPIO_METHOD_IOPinConfig io_pin_config;
        struct PCGPIO_METHOD_WriteDigital write_digital;
        struct PCGPIO_METHOD_WriteAnalog write_analog;
        struct PCGPIO_METHOD_ReadDigital read_digital;
        struct PCGPIO_METHOD_ReadAnalog read_analog;
        struct PCGPIO_METHOD_GetPinConfig get_pin_config;
    } method;
};

#endif
